#TASK F1 LIST

#Functionality

The App shows a list of F1 champions of the years between 2005-2015. Clicking on a champions row will show a list of races results for that season with the winner of the race, round and the race name. If the winner of the race won the championship that season his name will be hightlighted in red with champion icon. 


#Architecture

The app is a SPA built with Angular 2/4. 

It has a main AppComponent which controls the data shown on the page and two presentational components ChampionsListComponent and SeasonResultsComponent that display their corresponding data. Two services were implemented to fetch and filter the data obtainer from the APIService and DataFiltersService.

The data flows from the main component to it's children, presentational components send events through their outputs to notify the parent that data updates are required. 

Environment variables were set to control presets values for pagination and champions list results size and offset.

#RUN 

Make sure you have the [Angular CLI](https://github.com/angular/angular-cli#installation) installed globally, then run `npm install` to resolve all dependencies (might take a minute).

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`.

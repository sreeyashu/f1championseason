import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';

import { MaterialModule } from './core/modules/app-import-material.module';
import { AppComponent } from './app.component';
import { ApiService } from './api-service/services/app.services';
import { DataFiltersService } from './api-service/services/content-filter.service';
import { ChampionsListModule } from './shared/components/driver-list/driver-list.component';
import { SeasonResultsModule } from './shared/components/season-list/season-list.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MaterialModule,
    ChampionsListModule,
    SeasonResultsModule
  ],
  providers: [
    ApiService,
    DataFiltersService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

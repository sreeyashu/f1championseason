import { Component, NgModule, Input, OnChanges, ViewChild, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatPaginator } from '@angular/material';

import { MaterialModule } from '../../../core/modules/app-import-material.module';
import { FilteredSeasonResults } from '../../../api-service/models/app.model';
import { SeasonResultsOptions } from '../../models/season-results.model';

@Component({
  selector: 'app-season-results',
  templateUrl: 'season-list.component.html',
  styleUrls: ['season-list.component.scss']
})
export class SeasonResultsComponent implements OnChanges {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @Input() seasonResults: FilteredSeasonResults;
  @Input() options: SeasonResultsOptions;
  @Input() championData: any;
  @Output() paginatorChanged = new EventEmitter<any>();

  ngOnChanges(changes: SimpleChanges) {
    if (changes.championData) {
      this.resetPagination();
    }
  }

  pageChanged($event) {
    this.paginatorChanged.emit($event);
  }

  resetPagination() {
    if (this.paginator) {
      this.paginator.pageIndex = 0;
      this.paginator.pageSize = this.options.pageSizeOptions[0];
    }
  }
}

@NgModule({
  imports: [MaterialModule, CommonModule],
  declarations: [SeasonResultsComponent],
  exports: [SeasonResultsComponent]
})
export class SeasonResultsModule { }
